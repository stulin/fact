CXX=g++
EXE=build/factorial
SOURCES=$(shell find source -name "*.cpp")
OBJECTS:=$(subst .cpp,.o,$(SOURCES))
OBJECTS:=$(subst source,build,$(OBJECTS))

build/%.o: source/%.cpp
	mkdir -p build/
	$(CXX) $< -c -o $@

$(EXE): $(OBJECTS)
	mkdir -p build/
	$(CXX) $^ -o $(EXE)

clean:
	rm -rf build/

install:
	install $(EXE) /usr/local/bin/
