#include "factorial.hpp"

unsigned long factorial(unsigned int value) {
    unsigned long result = 1;
    for (unsigned int elem = 2; elem <= value; elem++) {
        result *= elem;
    }
    return result;
}

