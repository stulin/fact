#include <iostream>
#include "factorial.hpp"

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Wrong usage." << std::endl;
        return 1;
    }
    int value = atoi(argv[1]);
    std::cout << factorial(value) << std::endl;
    return 0;
}
